import { Client} from "./client";

export abstract class Component{
  
  protected container:JQuery;

  public constructor(protected readonly client:Client, containerSelector:string){
    this.container = $(containerSelector);
  }


  public deactivate(fast:boolean = false){
    if (fast)
      this.container.hide();
    else
      this.container.slideUp();
  }

  
  public activate(fast:boolean = false){
    if (fast)
      this.container.show();
    else
      this.container.slideDown();
  }


}