export namespace Constants{
  export const defaultRoomRefreshRate = 5000;
  export const boardWidth = 640;
  export const boardHeight= 480;


  //20 times a second
  export const broadcastRate = 50;

}