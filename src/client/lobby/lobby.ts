import { Client} from "../client";
import { Component } from "../Component";
import { UserForm} from "./userform";
import { RoomList} from "./roomlist";
import { EventNames,  Result } from "../../common/shared";
import {Comms} from "../../common/comms";


export class Lobby extends Component{

  private readonly userForm:UserForm;
  private readonly roomlist:RoomList;


  public constructor(client:Client, container:string = "#lobby"){
    super(client, container);
    this.roomlist = new RoomList(this);
    this.userForm = new UserForm(this);
  }

  public activate(fast:boolean = false){
    this.roomlist.update();
    super.activate(fast);
  }

  public deactivate(fast:boolean = false){
    this.roomlist.stop();
    super.deactivate(fast);
  }

  /**
   * request room join
   */
  public requestJoinRoom(roomId:string){
    if (this.userForm.validate()){
      //try to join room
      console.log(`attempting to join ${roomId}` );
      this.client.socket.emit(EventNames.JoinRoomRequest, { room: roomId, name: this.userForm.name, password:this.userForm.password });
    }
  }

  public requestCreateRoom(name:string, password:string){
    console.log(`attempting to create room` );
      this.client.socket.emit(EventNames.CreateRoomRequest, { name: this.userForm.name, password:this.userForm.password });
  }

}