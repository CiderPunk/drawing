import { Comms } from "../../common/comms";
import { Lobby } from "./lobby";
import { Constants} from "../consts";

export class RoomList{

  private roomlist:JQuery;
  private tbody:JQuery;
  private timer:NodeJS.Timer;

  public constructor(private owner:Lobby){
    this.roomlist = $('#room-list');
    this.tbody = this.roomlist.find('tbody');
    let self = this;
    this.tbody.on("click", ".join-button",(e:Event)=>{ 
      let roomId = $(e.target).attr('data-room');
      this.owner.requestJoinRoom(roomId);
    });
    $("#room-list-refresh").click((e:Event)=>{
       this.update();
    })

    this.update();
  }

  public update(rate:number= Constants.defaultRoomRefreshRate){
    this.stop();
    $.ajax("/api/getrooms", { method:"GET", cache:false,  }).done((r:Comms.Room[]) => {
      this.tbody.empty();
      r.forEach((room)=>{
        let row = $(`<tr>
            <td>${room.name} ${room.passworded ? '<span class="glyphicon glyphicon-lock"></span>' : ''}</td>
            <td class="text-center">${room.playerCount}</td>
            <td class="text-right"><input type="button" class="btn btn-default btn-sm join-button" value="join" data-room="${room.id}"/></td>
          </tr> `);
        this.tbody.append(row);
      });
    });
    this.timer = setTimeout(()=>{this.update(rate); }, rate);
  };

  public stop(){
    clearTimeout(this.timer);
  }

}