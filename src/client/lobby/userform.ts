import {Lobby} from "./lobby";
import {PersistControl} from "../helpers/persistcontrol";
import { Notice } from "../ui/notice";

export class UserForm{
  private readonly form:JQuery;
  private readonly usernameInput:JQuery;
  private readonly passwordInput:JQuery;
  private readonly notice:Notice;

  public constructor(private owner:Lobby){
    this.notice = new Notice("#user-details-notice");
    this.form = $("#form-user-details"); 
    this.usernameInput = this.form.find('#user-details-name');
    this.passwordInput = this.form.find('#user-details-password');
    this.form.submit((e:Event)=>{ this.createGame(e);});
    //set up persistance controls
    PersistControl();  
  }

  public get name(): string{
    return this.usernameInput.val();
  }

  public set name(val:string){
    this.usernameInput.val(val);
  }

  public get password(): string{
    return this.passwordInput.val();
  }

  createGame(e:Event){
    e.preventDefault();
    if (this.validate()){
      this.owner.requestCreateRoom(this.name, this.password);
    }
  }

  validate():boolean{
    let isValid = (this.name.trim() != "");
    this.usernameInput.parent().toggleClass("has-error", !isValid);
    if (!isValid)
    this.notice.ShowNotice("Please enter your name");
    return isValid;
  }

}