import { Lobby } from "./lobby/lobby";
import { RoomClient } from "./room/roomclient";
import * as Shared from "../common/shared";
import {Comms} from "../common/comms";
import {Init} from "./init";
import { Notice} from "./ui/notice";


export class Client{
  public readonly socket:SocketIOClient.Socket;
  private readonly lobby:Lobby;
  private readonly room:RoomClient;
  public readonly notice:Notice;

  public constructor(){
    //initialize stuff
    Init();
    this.socket = io();
    this.lobby = new Lobby(this);
    this.room = new RoomClient(this)
    //hide the game to start
    this.room.deactivate(true);

    this.notice = new Notice("#notice-container");


    this.socket.on(Shared.EventNames.JoinRoomResponse, (msg:Comms.JoinRoomResponse) => { 
      this.switchToRoom(msg);
    });

    this.socket.on(Shared.EventNames.Error, (msg:Comms.Error) => { 
      this.notice.ShowResult(msg.result)
    });


  }

  public switchToRoom(resp:Comms.JoinRoomResponse){
    this.lobby.deactivate();  
    this.room.setRoom(resp);
    this.room.activate();
  }

  public leaveRoom(){
    this.socket.emit(Shared.EventNames.LeaveRoomRequest);
    this.room.deactivate();
    this.lobby.activate();
  }

}
