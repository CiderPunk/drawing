import { IGameClient, IRoomClient } from "../interfaces";
import { Registry } from "../registry";
import {Comms} from "../../common/comms";
import * as Shared from "../../common/shared";



export class DrawClubClient implements IGameClient{

  //register this game client
  public static  reg(){
    Registry.registerGameClient("drawclub", DrawClubClient.instance);
  }

  public static readonly instance:DrawClubClient = new DrawClubClient();
  private constructor(){}

  private room:IRoomClient;

  public init(room:IRoomClient){
    this.room = room;
  }

  public handleGameState(msg:Comms.GameState){
    switch(msg.state){
      case Shared.GameState.WaitingForPlayers:
        this.room.draw.disable();
        this.room.chat.addGameNotice("Waiting for players", 0, Shared.Importance.Info);
        break;
      case Shared.GameState.MatchStarting:
        this.room.draw.disable();
        this.room.chat.addGameNotice("Game starting soon", msg.time, Shared.Importance.Info);
        break;
      case Shared.GameState.RoundStarting:
        this.room.draw.disable(); 
        this.room.chat.addGameNotice(`Round starting ${msg.session.name} to draw next`, msg.time, Shared.Importance.Info);
        break;

    }
  }


}


