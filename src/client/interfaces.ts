
import {Comms} from "../common/comms";
import * as Shared from "../common/shared";


export interface IGameClient{
  init(room:IRoomClient):any;
  handleGameState(msg:Comms.GameState):any;
}

export interface IRoomClient{
  sendDrawing(toSend:number[]):any;
  sendChat(message:string):any;
  draw:IDrawingBoard;
  chat:IChatDisplay;
  chatForm:IChatForm;
}

export interface IChatDisplay{
  addGameNotice(message:string, time:number, imp:Shared.Importance):any;
  addGameNotice(message:string, time:number):any;
}

export interface IChatForm{

}

export interface IDrawingBoard{
  clear():any;
  disable():any;
  enable():any;
  setEnabled(state:boolean):any;
}