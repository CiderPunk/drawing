export function PersistControl(selector:string = ".persis-control"){
  $(selector).each(function(){ 
    let $this = $(this);
    let key = "pc-" + $this.attr('name');
    $this.val(localStorage.getItem(key));
    $this.change((e:Event)=>{
      localStorage.setItem(key, $this.val());
    });
  });
}
