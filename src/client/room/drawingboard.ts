import {Constants} from "../consts";
import {IRoomClient} from "../interfaces";
import {Drawer} from "./drawer";
import {IDrawingBoard} from "../interfaces";

export class DrawingBoard implements IDrawingBoard{

  private scale = 1;
  private enabled:boolean;
  private tracking:boolean;
  //private drawbuffer:number[] = [];
  //private recievedBuffer:number[] = [];
  private readonly ctx:CanvasRenderingContext2D;
  private readonly canvas:HTMLCanvasElement;
  private readonly container:HTMLElement;
  private enableDrawing:boolean = true;
  private localDrawer:Drawer;
  //private broadcastPoint = 0;

  private drawers = new Map<string,Drawer>();

  public constructor(private room:IRoomClient, selector:string = "canvas-container"){
    this.container = document.getElementById(selector);
    this.canvas = document.createElement("canvas");
    this.container.appendChild(this.canvas);
    this.ctx = (this.canvas as HTMLCanvasElement).getContext('2d');
    this.localDrawer = new Drawer(this.ctx);

    this.canvas.onmousedown = (ev:MouseEvent)=>{
      ev.preventDefault();
      //if lefty... 
      if (ev.button == 0){
        this.tracking = true;
        this.addDraw(ev.offsetX, ev.offsetY, true);
      }
    };
    window.onmouseup = (ev:MouseEvent)=>{
      //lefty check
      if (ev.button == 0 && this.tracking){
        this.tracking = false;
        this.addStop();
      }
    };

    this.canvas.onmousemove = (ev:MouseEvent)=>{
      if (this.tracking){
        this.addDraw(ev.offsetX, ev.offsetY);
      }
    };

    //recalculate size on window resize
    window.onresize = (e:UIEvent)=>{
      this.calcSize();  
    }

    //init size
    this.calcSize();

    //start broadcasting interval
    setInterval(()=>{ this.broadcast()}, Constants.broadcastRate);
  }

  public enable(){
    this.setEnabled(true);
  }

  public disable(){
    this.setEnabled(false);
  }
  public setEnabled(state:boolean){
    this.enabled = state;
  }

  private getDrawer(id:string){
    let drawer = this.drawers.get(id);
    if (drawer === undefined){
      drawer = new Drawer(this.ctx);
      this.drawers.set(id, drawer);
    }
    return drawer;
  }

  private broadcast(){
    //check drawing is enabled 
    if (this.enableDrawing){
      let toSend = this.localDrawer.getBroadcast();
      if (toSend.length > 0){
        this.room.sendDrawing(toSend);
      }
    }
  }

  private calcSizeTimer: NodeJS.Timer;
  public calcSize(){
    clearTimeout(this.calcSizeTimer);
    this.calcSizeTimer = setTimeout(()=>{
      //container width height
      let cw = this.container.clientWidth;
      //max height is a percentage of client window size
      let ch = window.innerHeight * 0.6;
      //canvas ratio
      let ratio = cw / ch;
      //desired ratio
      let desiredratio = Constants.boardWidth / Constants.boardHeight; 
      if (ratio > desiredratio){
        //need vertical slice  
        this.canvas.width = desiredratio * ch;
        this.canvas.height = ch;
      }
      else{
        //need a horizontal slice!
        this.canvas.width =  cw;
        this.canvas.height = cw / desiredratio ;
      }
      this.scale  = Constants.boardWidth / this.canvas.width;
      this.ctx.setTransform(1/this.scale,0,0, 1/this.scale,0,0);
      this.redraw();
    }, 100);
  }

  private addStop(){
    if (this.enableDrawing)
      this.localDrawer.drawSegment([-1]);
  }



  private addDraw(x:number, y:number, start:boolean = false){
    if (!this.enableDrawing)
      return;
    let px = x * this.scale;
    let py = y * this.scale;
    this.localDrawer.drawSegment([px,py]);
  }

  public drawBroadcast(id:string, points:number[]){
    this.getDrawer(id).drawSegment(points);
  }

  private redraw(){
    this.localDrawer.drawAll();

    this.drawers.forEach((value:Drawer, key:string) =>{
      value.drawAll();
    });
  }

  public clear(){
    this.drawers.forEach((value:Drawer, key:string) =>{
      value.clear();
    });

  }

}