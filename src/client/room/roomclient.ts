import {ChatDisplay} from "./chatdisplay";
import {ChatForm} from "./chatform";
import {PlayerList} from "./playerList";
import {DrawingBoard} from "./drawingboard";

import {Client} from "../client";
import {Component} from "../component";
import * as Shared from "../../common/shared";
import {Comms} from "../../common/comms";
import {IRoomClient, IGameClient} from "../interfaces";
import {Registry} from "../registry";


export class RoomClient extends Component implements IRoomClient{

  public readonly chat:ChatDisplay;
  public readonly chatForm:ChatForm;
  private readonly playerList:PlayerList;
  public readonly draw:DrawingBoard;

  private readonly gameContainer:JQuery;
  private roomId:string;
  private playerId:string;

  private game:IGameClient;


  public constructor(client:Client, container:string = "#game"){
    super(client, container);
    this.draw = new DrawingBoard(this);
    this.chat = new ChatDisplay();
    this.chatForm = new ChatForm(this);
    this.playerList = new PlayerList(); 

    this.client.socket.on(Shared.EventNames.BroadcastMessage, (msg:Comms.RoomChatMessage) => { 
      this.chat.addMessage(msg.name, msg.message)
    });
    this.client.socket.on(Shared.EventNames.BroadcastNotice, (msg:Comms.RoomNotice) => { 
      this.chat.addNotice(msg);
      if (msg.refreshUser){
        this.playerList.update(this.roomId);
      }
    });
    this.client.socket.on(Shared.EventNames.BroadcastDrawing, (msg:Comms.DrawSegment)=>{
      //only show drawings from other users...
      if (msg.user != this.playerId){
        this.draw.drawBroadcast(msg.user, msg.points);
      }
    });
    this.client.socket.on(Shared.EventNames.GameState, (msg:Comms.GameState)=>{
      console.log(msg);
      if (this.game != null)
        this.game.handleGameState(msg);
    })

    $("#leave-room").click(()=>{ this.client.leaveRoom(); })

  }

  public setRoom(resp:Comms.JoinRoomResponse){
    console.log("Joined room " + resp.id);
    this.playerId = resp.playerId;
    this.roomId = resp.id;
    this.container.find("#room-title").html(resp.name);
    this.playerList.update(resp.id);
    this.chat.clear();
    this.draw.clear();
    //make sure draw panel is resized right...
    this.draw.calcSize();
    //set title./..
    this.game = Registry.getGameClient(resp.game);
    this.game.init(this);
  }

  public sendChat(msg:string){
    this.client.socket.emit(Shared.EventNames.SendMessage, msg);
  }

  public sendDrawing(points:number[]){
    this.client.socket.emit(Shared.EventNames.SendDrawing, points);
  }





}