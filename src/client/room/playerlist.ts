
import { Comms } from "../../common/comms";


export class PlayerList{

  private readonly userTable:JQuery;

  public constructor(selector:string = "#player-list tbody"){
    this.userTable = $(selector);
  }

  public update(roomId:string){
    $.ajax(`/api/getplayers/${roomId}`, { method:"GET", cache:false }).done((r:Comms.Player[]) => {
       this.userTable.empty();
      r.forEach((player)=>{
        let row = $(`<tr>
            <td>${player.name}</td><td  class="text-right">${player.score}</td>
          </tr> `);
        this.userTable.append(row);
      });
    });

  }
}