import {Importance, ImportanceText, NoticeType} from "../../common/shared";
import {Comms} from "../../common/comms";
import { IChatDisplay} from "../interfaces";

export class ChatDisplay implements IChatDisplay{

  private readonly container:JQuery;
  public constructor(container:string = "#chat-display"){
    this.container = $(container);
  }

  public addMessage(name:string, message:string){
    this.container.prepend($(`<li class="list-group-item"><strong>${name}</strong> ${message}</li>`));
  }

  public addGameNotice(message:string, time:number, imp:Importance = Importance.Info){
     this.container.prepend($(`<li class="list-group-item list-group-item-${ImportanceText(imp)}">${message}</li>`));
  }

  public addNotice(msg:Comms.RoomNotice){
    let imp:Importance = Importance.Success;
    let text:string;
    switch (msg.type){
      case NoticeType.RoomJoin:
        text = `<strong>${msg.name}</strong> joined the room`;
        break;
      case NoticeType.RoomLeave:
        text = `<strong>${msg.name}</strong> left the room`;
        break;
      default:
        text="TBC";        
        break;
    }
    this.container.prepend($(`<li class="list-group-item list-group-item-${ImportanceText(imp)}">${text}</li>`));
  }

  public clear(){
    this.container.empty();

  }

  //TODO prune old chats

}