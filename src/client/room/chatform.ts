import { IRoomClient } from "../interfaces";

export class ChatForm{
  private readonly form:JQuery;
  private readonly textfield:JQuery;

  public constructor(private room:IRoomClient,  selector:string = "#chat-form"){
    this.form = $(selector);
    this.textfield = this.form.find("#chat-text");
    this.form.submit((e:Event)=>{ 
      e.preventDefault();
      this.sendChat();
    });
  }

  public sendChat(){
    let text = this.textfield.val();
    if (text.trim() !== ""){
      this.room.sendChat(text);
    }
    //clear the input
    this.textfield.val("");
  }

}