export class Drawer{
  
  private drawbuffer:number[] = [];
  private transPos:number = 0;
  private drawPos:number = 0;

  private penDown:boolean = false;
  private lastX:number;
  private lastY:number;

  public constructor( private ctx:CanvasRenderingContext2D, private strokeStyle:string = "black"){

  }

  public getBroadcast(){
    let toSend = this.drawbuffer.slice(this.transPos, this.drawbuffer.length);
    this.transPos = this.drawbuffer.length;
    return toSend;
  }

  public clear(){
    this.drawbuffer = [];
  }

  public drawSegment(points:number[]){
    this.draw(points);
    this.drawbuffer = this.drawbuffer.concat(points);
  }

  public drawAll(){
    this.penDown = false;
    this.draw(this.drawbuffer);
  }

  private draw(points:number[]){
    let i = 0;
    this.ctx.beginPath();
    if (this.penDown){
      this.ctx.moveTo(this.lastX,this.lastY);
    }
    while(i < points.length){
      if (points[i] == -1){
        this.penDown = false;
      }  
      else{
        this.lastX = points[i];
        this.lastY = points[++i];
        if (!this.penDown){
          this.ctx.moveTo(this.lastX,this.lastY);
          this.penDown = true;
        }
        else{
          this.ctx.lineTo(this.lastX,this.lastY);
        }
      }
      i++;
    }    
    this.ctx.stroke();
  }


}