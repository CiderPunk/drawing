import{Result , Importance, ImportanceText} from "../../common/shared";
/**
 * handles result messages for user
 */
export class Notice{

  private readonly container:JQuery;
  static readonly persistTime = 3000;

  public constructor(container:string){
    this.container = $(container);
  }

  public GetResultText(res:Result):string{
    switch(res){
      case Result.Success:
        return "Success";
      case Result.RoomFull:
        return "Game full";
      case Result.WrongPassword:
        return "Incorrect password";
      case Result.Error:
        return "Error";
      case Result.RoomNotFound:
        return "Room not found";
      case Result.TooManyRooms:
        return "Too many rooms, Cannot create new ones"
    }
    return `Error: ${res}`;
  }

  public ShowNotice(message:string, severity:Importance = Importance.Danger, time =  Notice.persistTime){
    let $message = $(`<div class="alert alert-${ImportanceText(severity)}" role="alert">${message}</div>`).hide();
    this.container.prepend($message);
    $message.slideDown();
    //hide after a while
    setTimeout(()=>{ 
      $message.slideUp().promise().done(()=>{ $message.remove(); });
    }, time);
  }

  public ShowResult(res:Result, severity:Importance = Importance.Danger, time =  Notice.persistTime){
    let $message = $(`<div class="alert alert-${ImportanceText(severity)}" role="alert">${this.GetResultText(res)}</div>`).hide();
    this.container.prepend($message);
    $message.slideDown();
    //hide after a while
    setTimeout(()=>{ 
      $message.slideUp().promise().done(()=>{ $message.remove(); });
    },  time);
  }
}

export enum AlertSeverity{
  Success,
  Info,
  Warning,
  Danger,
}
