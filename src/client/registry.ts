
import {IGameClient } from "./interfaces";
export namespace Registry{

  let clients = new Map<string, IGameClient>();

  export function registerGameClient(name:string, client:IGameClient){
    clients.set(name, client);
  }
  
  export function getGameClient(name:string):IGameClient{
    return clients.get(name);
  }

}