

export enum GameState{
  WaitingForPlayers = 1,
  MatchStarting = 2,
  RoundStarting = 4,
  InGame  = 8,
  NotPlaying = WaitingForPlayers | MatchStarting,
  Playing = RoundStarting | InGame,
}

export enum Result{
  Success,
  WrongPassword,
  RoomFull,
  RoomNotFound,
  UserAlreadyInRoom,
  Error,
  TooManyRooms,
}

export enum NoticeType{
  RoomJoin,
  RoomLeave,
  RoundWon,
  RoundLost,
}


export enum Importance{
  Info,
  Warning,
  Success,
  Danger,
}

export function ImportanceText(val:Importance){
  switch(val){
    case Importance.Info:
      return "info";
    case Importance.Warning:
      return "warning";
    case Importance.Success:
      return "success";
    case Importance.Danger:
      return "danger"; 
  }
  return "unknown";
} 

export namespace EventNames{
  export const CreateRoomRequest = "creq";
  export const JoinRoomRequest = "jreq";
  export const JoinRoomResponse = "jrsp";
  export const LeaveRoomRequest = "lreq";
  export const SendMessage = "sc";
  export const BroadcastMessage = "bc";
  export const BroadcastNotice = "rn";
  export const SendDrawing = "sd";
  export const BroadcastDrawing = "bd"
  export const GameState = "gs";
  export const Error = "er";
}
