
import * as Shared from "./shared";

export namespace Comms{

  export interface GameState{
    state:Shared.GameState,
    time:number,
    session:GameSession
  }

  export interface GameSession{
    name:string

  }

  export interface Error{
    result:Shared.Result,
    message:string
  }


  export interface CreateRoomRequest{
    name:string,
    password:string
  }

  export interface JoinRoomRequest{
    room:string,
    name:string,
    password:string
  }

  export interface JoinRoomResponse{
    result:Shared.Result,
    id:string,
    playerId:string,
    name:string,
    game:string
  }

  export interface RoomChatMessage{
    name:string,
    message:string,
  }

  export interface RoomNotice{
    type:Shared.NoticeType,
    name:string,
    refreshUser:boolean
  }

  export interface DrawSegment{
    user:string,
    points:number[]
  }

  export interface Room{
    id:string;
    name:string;
    passworded:boolean;
    playerCount:number;
  }

  export interface Player{
    name:string;
    score:number;
  }

}