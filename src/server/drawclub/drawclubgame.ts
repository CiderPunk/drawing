import { IGame, IRoom, IUser, IWordSource, IChatEvent, IDrawEvent } from "../interfaces";
import * as Shared from "../../common/shared";
import { Comms } from "../../common/comms";
import { Round } from "./round";


const MatchStartTime = 15000;
const RoundStartTime = 5000;
const RoundTime = 60000;
const MinPlayers = 2;

export class DrawClubGame implements IGame{

  private room:IRoom;
  private state:Shared.GameState;
  private round:Round = null;
  private nextStateTime:number;
  private timer:NodeJS.Timer; 

  public readonly gameClient:string = "drawclub"; 


  public constructor(private wordsource:IWordSource){    
    this.state = Shared.GameState.WaitingForPlayers;
  }

  public init(room:IRoom) {
    this.room = room;
  }

  public dropPlayer(user:IUser){
    //TODO: check we have enough players to play
    if (this.room.userCount < MinPlayers){
      this.endMatch();
    } 
    //TODO: Check current drawer hasn't left
    if (this.round !== null && this.round.drawer.id === user.id){
      //this.round.abbandon();
    }
  }

  public addPlayer(user:IUser){
    //TODO: check if player count is sufficient to start a game
    if (this.state == Shared.GameState.WaitingForPlayers && this.room.userCount >= MinPlayers){
      //start game with random drawer...
      this.startMatch();
    } 
    //send current game state
    user.sendGameState(this.gameState );
  }

  public draw(e:IDrawEvent){
    if (this.state != Shared.GameState.InGame)
      return;
    if (e.user.id != this.round.drawer.id){
      //drawer cannot chat
      e.broadcast = false;
      return;
    }
    //TODO: update last activity timeout on player...
    
  }

  public chat(e:IChatEvent){
    if (this.state != Shared.GameState.InGame)
      return;
    if (e.user.id == this.round.drawer.id){
      //drawer cannot chat
      e.broadcast = false;
    }
  }

  private setNextStateTime(msecs:number){
    this.nextStateTime = Date.now() + msecs;
  }

  private get timeToNextState():number{
    return  this.nextStateTime - Date.now();
  }

  private get gameState():Comms.GameState{
    let ses:Comms.GameSession = ((this.state | Shared.GameState.NotPlaying) == Shared.GameState.NotPlaying) ? null :
      { name: this.round.drawer.name };
    return { state: this.state, time: this.timeToNextState, session: ses };
  }

  private startMatch(){
    this.state = Shared.GameState.MatchStarting;
    this.setNextStateTime(MatchStartTime);
    this.room.sendGameState( this.gameState );
    clearTimeout(this.timer);
    this.timer = setTimeout(()=>{
      this.startRound(this.pickRandomDrawer());
    }, MatchStartTime);
  }


  private endMatch(){
    this.state = Shared.GameState.WaitingForPlayers;
    this.setNextStateTime(0);
    //update players
    this.room.sendGameState( this.gameState );
  }



  private startRound(drawer:IUser){
    this.state = Shared.GameState.RoundStarting;
    this.setNextStateTime(RoundStartTime);
    this.round = new Round(drawer, this.wordsource.getNext());
    //update players
    this.room.sendGameState( this.gameState );
  }

  private pickRandomDrawer():IUser{
    return this.room.users[Math.floor(Math.random() * this.room.userCount)];
  }

}