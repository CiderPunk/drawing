import {IUser} from "../interfaces";

export class Round{

  private p:number[];

  public constructor(public drawer:IUser, private word:string){

  }

  public addPoints(points:number[]){
    this.p = this.p.concat(points);
  }

  public get points():number[]{
    return this.p;
  }

}