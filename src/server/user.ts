import { Server } from "./server";
import { Room } from "./room";
import { Utils  } from "./utils"
import * as Io from "socket.io";
import { EventNames, Result } from "../common/shared";
import { Comms } from "../common/comms";
import { IUser } from "./interfaces";


export class User implements IUser{

  public name:string;
  public score:number;
  private room:Room = null;

  public get id(): string{
    return this.socket.id;
  }


  constructor(private server:Server, private readonly socket:SocketIO.Socket){
    this.socket.on('disconnect', ()=>{ this.disconnect(); } );
    console.log('a user connected: ' + this.id);

  //room join request...
    this.socket.on(EventNames.CreateRoomRequest, (req:Comms.CreateRoomRequest)=>{ 
      this.name = Utils.cleanInput(req.name);
      this.score = 0;
      let res = Result.Error;
      let room:Room = null;
      try{
        room = server.createRoom(req.name, req.password);
        res = room.addUser(this, req.password);
        this.socket.join(room.id);
        this.room = room;
      }
      catch(e){
        res = e as Result;
      }
      let resp:Comms.JoinRoomResponse = { playerId:this.id, id:room!= null ? room.id : "", name:room!= null ? room.name : "", result:res, game:room.gameClient}
      this.socket.emit(EventNames.JoinRoomResponse, resp);
    });

    //room join request...
    this.socket.on(EventNames.JoinRoomRequest, (req:Comms.JoinRoomRequest)=>{ 
      //TODO: check name is legit
      this.name = Utils.cleanInput(req.name);
      //reset score
      this.score = 0;
      let room = this.server.getRoom(req.room);
      //try to join room
      let res = room!= null ? room.addUser(this, req.password) : Result.RoomNotFound;
      if (res !== Result.Success){
        this.sendError(res)
      }
    });
    //chat message
    this.socket.on(EventNames.SendMessage, (msg:string)=>{  
      let chatEvent = {user:this, message:msg, broadcast:true}
      if (this.room)
        this.room.sendMessage(chatEvent);
    });
    //drawing
    this.socket.on(EventNames.SendDrawing, (points:number[])=>{
      let drawEvent = {user:this, points:points, broadcast:true}
      if (this.room)
        this.room.sendDrawing(drawEvent);
    });
    //quitting room
    this.socket.on(EventNames.LeaveRoomRequest, ()=>{
      if(this.room){
        this.socket.leave(this.room.id);
        this.room.dropUser(this);
        this.room = null;
      }
    });
  }

  disconnect(){
    console.log('a user disconnected: ' + this.id);
    if (this.room != null){
      this.room.dropUser(this);
      this.room = null;
    }    
    this.server.dropUser(this);
  }

  public sendGameState(state:Comms.GameState){
    this.socket.emit(EventNames.GameState, state);
  }

  public doJoinRoom(room:Room){
    this.room = room;
    this.socket.join(room.id);
    let resp:Comms.JoinRoomResponse = { playerId:this.id, id:room.id, name:room.name, result:Result.Success, game:room.gameClient}
    this.socket.emit(EventNames.JoinRoomResponse, resp);
  }

  public sendError(res:Result, message:string = null){
    this.socket.emit(EventNames.Error, { result:res, message:null});
  }

}