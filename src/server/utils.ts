export namespace Utils{
  export function generateId(count = 32, chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"):string{
    let id = "";
    for(let i =0; i < count; i++){
      id += chars.substr(Math.floor(Math.random() * chars.length), 1);
    }
    return id;
  }

  export function cleanInput(val:string){
    return val
      .replace(/&/g, '&amp;')
      .replace(/"/g, '&quot;')
      .replace(/'/g, '&#39;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;').trim();
  }



}