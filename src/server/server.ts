import * as Http from "http";
import * as Express  from "express";
import * as Io from "socket.io";
import { User } from "./user";
import { Room } from "./room";
import {Constants } from "./consts"
import { JsonWordSource } from "./wordsource/jsonwordsource";
import { DrawClubGame } from "./drawclub/drawclubgame";
import { EventNames, Result } from "../common/shared";
import { Comms } from "../common/comms";



export class Server{
  private readonly router:Express.Router;
  private readonly app:Express.Express;
  private readonly server:Http.Server;
  public readonly io:SocketIO.Server;

  private rooms = new Map<string,Room>();
  private users = new Map<string,User>();

  constructor(port:number = process.env.PORT || 80){
    this.app  = Express();
    //this.app.use('/src', Express.static('src'));
    this.app.use('/node_modules', Express.static('node_modules'));
    this.app.use(Express.static('public'));
    this.server = this.app.listen(port,function(){
      console.log('listening on *:' + port);
    } );


    //init restful services
    this.router = Express.Router();
    this.router.get('/getrooms',(req,res,next)=>{ 
      res.json(this.getRoomList()); 
    });
    this.router.get('/getplayers/:room',(req,res,next)=>{ 
      let room = this.getRoom(req.params.room);
      if (room!==null)
        res.json(room.getPlayerList())
    });

    this.app.use('/api', this.router);

    //init socket.io
    let self:Server = this;
    this.io = Io(this.server);
    this.io.on('connection',function(socket){
      let user = new User(self, socket)
      self.users.set(user.id, user);
    });
  }


  public createRoom(uname:string, password:string ):Room{
    if (this.rooms.size >= Constants.MaxRooms)
      throw Result.TooManyRooms;
    let room = new Room(this, uname, password, new DrawClubGame( new JsonWordSource("./data/words.json")));
    this.rooms.set(room.id, room);
    return room;
  }

  public getRoom(id:string):Room{
    return this.rooms.get(id);
  }

  public getRoomList():Comms.Room[]{
    let result:Comms.Room[] = [];
    this.rooms.forEach((value:Comms.Room, key:string) =>{
      result.push({ id:value.id, name: value.name, passworded:value.passworded, playerCount:value.playerCount});
    });
    return result;
  }


  /**
   * user disconnected
   */
  public dropUser(user:User){
    this.users.delete(user.id);
  }
  public dropRoom(room:Room){
    this.rooms.delete(room.id);
  }


}