
import {IWordSource } from "../interfaces";

export class JsonWordSource implements IWordSource{
  
  private wordlist:string[];
  private prevWords:string[] = [];

  public constructor(path:string, private repeatPrevent:number = 5){
    let fs = require('fs');
    let file = fs.readFileSync(path, 'utf8');
    let data = JSON.parse(file);
    this.wordlist = data as string[];

    if (this.wordlist === null){
      throw "Invalid word list";
    }
    //prevent infinite loopings!
    if (this.repeatPrevent > this.wordlist.length){
      this.repeatPrevent= this.wordlist.length - 1;
    }
  }

  public getNext():string{
    while(true){
      let word = this.wordlist[Math.floor(Math.random() * this.wordlist.length)];
      if (this.prevWords.indexOf(word) < 0){ // new word
        this.prevWords.push(word);
        if (this.prevWords.length > this.repeatPrevent){
          //if we have more than our previous store
          this.prevWords.shift();
        }
        return word;
      }
    }
  }


}