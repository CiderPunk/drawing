import { Utils } from "./utils";
import { User } from "./user";
import { Server } from "./server";
import { Result, EventNames,  NoticeType, GameState } from "../common/shared";

import {Comms} from "../common/comms";
import {Constants} from "./consts";

import {IWordSource, IRoom, IGame, IDrawEvent, IChatEvent} from "./interfaces";

import * as Io from "socket.io";

export class Room implements IRoom{

  public id:string;
  public name:string;
  public passworded:boolean;
  private password:string;
  public users:User[] = [];

  public constructor(private owner:Server, creatorName:string, password:string, private game:IGame){
    this.id = Utils.generateId(20);
    this.name = creatorName + "'s room";
    this.password = password ? password.trim() : null;
    this.passworded = (this.password != null && this.password.length > 0);
    //init the game
    this.game.init(this);
  }

  public dropUser(user:User){
    for(let i = 0; i < this.users.length; i++){
      if (this.users[i].id === user.id){
        this.users.splice(i,1);
      }
    }
    let notice: Comms.RoomNotice = {type:NoticeType.RoomLeave, name:user.name, refreshUser:true };
    this.owner.io.in(this.id).emit(EventNames.BroadcastNotice, notice);
    //notify game manager
    this.game.dropPlayer(user);

    //TODO: check if empty
    if (this.users.length == 0){
      this.owner.dropRoom(this);
    }
  }

  public get playerCount(): number{
    return this.userCount;
  }  
  
  public get userCount(): number{
    return this.users.length;
  }

  public get gameClient():string{
    return this.game.gameClient;
  } 

  public addUser(user:User, password:string ): Result{
    if (this.users.length >= Constants.MaxRoomSize){
      return Result.RoomFull;
    }
    //check matching password
    if (this.passworded && password !== this.password){
      return Result.WrongPassword;
    }
    //check user is not already in the room!
    for (let member of this.users){
      if (member == user){ 
        return Result.UserAlreadyInRoom;
      }
    }
    
    user.doJoinRoom(this);
    this.users.push(user);


    let notice: Comms.RoomNotice = {type:NoticeType.RoomJoin, name:user.name, refreshUser:true};
    this.owner.io.in(this.id).emit(EventNames.BroadcastNotice, notice);

    this.game.addPlayer(user);

    return Result.Success;
  }

  public sendMessage(e:IChatEvent){
    this.game.chat(e);
    if (e.broadcast){
      this.owner.io.in(this.id).emit(EventNames.BroadcastMessage, {name:e.user.name, message:e.message} );
    }
  }


  /**
   * sends drawing to client
   */
  public sendDrawing(e:IDrawEvent){
    //pass to the game manager...
    this.game.draw(e);
    //if no objection, broadcast
    if (e.broadcast)
      this.owner.io.in(this.id).emit(EventNames.BroadcastDrawing, {user:e.user.id,  points:e.points} );
  }
  
  public getPlayerList():Comms.Player[]{
    let res:Comms.Player[] = [];
    for (let member of this.users){
      res.push({ name: member.name, score: member.score})
    }
    return res;
  }


/**
 * send game state update to room
 */
  public sendGameState(state:Comms.GameState){
    this.owner.io.in(this.id).emit(EventNames.GameState, state);
  }



}