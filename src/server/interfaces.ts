import { Comms } from "../common/Comms";


export interface IGame{

  gameClient:string;
  init(room:IRoom):any;
  dropPlayer(user:IUser):any;
  addPlayer(user:IUser):any;
  draw(event:IDrawEvent):any;
  chat(event:IChatEvent):any;
}

export interface IRoom{
  userCount:number;
  users:IUser[];
  sendGameState(state:Comms.GameState):any; 
}

export interface IUser{
  id:string;
  name:string;
  sendGameState(state:Comms.GameState):any; 
}

export interface IWordSource{
  getNext():string;
}

export interface IDrawEvent{
  user:IUser;
  points:number[];
  broadcast:boolean;
}

export interface IChatEvent{
  user:IUser;
  message:string;
  broadcast:boolean;
}

